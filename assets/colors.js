export const gray = {
  offWhite: '#eee',
  lighter: '#dcdcdc',
  light: '#979797',
  medium: '#bababa',
  mediumDark: '#8c8c8c',
  dark: '#4a4a4a',
  darker: '#262626'
}
