import Minimal from './Minimal'
import FullInfo from './FullInfo'

export default {
  minimal: Minimal,
  full: FullInfo
}
