import {combineReducers} from 'redux'

import data from './data'
import feed from './feed'

export default combineReducers({data, feed})
