import * as home from './Landing'
import * as login from './Login'
import * as listing from './Listing'
import * as listings from './Listings'

export default {home, login, listing, listings}
